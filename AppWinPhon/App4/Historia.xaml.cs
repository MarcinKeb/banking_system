﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace App4
{
    public sealed partial class Historia : Page
    {
        private HttpClient httpClient;
        private HttpResponseMessage response;
        private String phpAddress = "https://serwer1505592.home.pl/historia.php?";
        string id = "";
        public Historia()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            httpClient = new HttpClient();
            var headers = httpClient.DefaultRequestHeaders;
            headers.UserAgent.ParseAdd("ie");
            headers.UserAgent.ParseAdd("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
        }
        
        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            id = e.Parameter.ToString();
            string phpStatus;
            phpAddress = "https://serwer1505592.home.pl/historia.php?IdKlienta=" + id;
            response = new HttpResponseMessage();
            Uri resourceUri;
            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }
            string responseText;
            try
            {
                response = await httpClient.GetAsync(resourceUri);
                response.EnsureSuccessStatusCode();
                responseText = await response.Content.ReadAsStringAsync();
            }
            catch (Exception)
            {
                responseText = "";
            }
            phpStatus = response.StatusCode + " " + response.ReasonPhrase;
            phpStatus = responseText.ToString();
            textBlock1.Text = phpStatus;
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(BlankPage2), id);
        }
    }
}
