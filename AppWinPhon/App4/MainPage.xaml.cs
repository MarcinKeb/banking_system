﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Net;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Http;

namespace App4
{
    
    public sealed partial class MainPage : Page
    {
        private HttpClient httpClient;
        private HttpResponseMessage response;
        private String phpAddress = "https://serwer1505592.home.pl/login.php?";

        public MainPage()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            httpClient = new HttpClient();
            var headers = httpClient.DefaultRequestHeaders;
            headers.UserAgent.ParseAdd("ie");
            headers.UserAgent.ParseAdd("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
        }
       
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            textBox.Text = "";
            textBox1.Text = "";
            phpStatus.Text = "";
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            
            phpAddress = "https://serwer1505592.home.pl/login.php?Nr_karty=" + textBox.Text + "&Haslo=" + textBox1.Text;
            response = new HttpResponseMessage();

            Uri resourceUri;
            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {               
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }
          
            string responseText;
            //phpStatus.Text = "Waiting for response ...";

            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();
                
            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }
            phpStatus.Text = response.StatusCode + " " + response.ReasonPhrase;

            phpStatus.Text = responseText.ToString();

            if ((phpStatus.Text != "ERROR PASSWORD") && (phpStatus.Text != ""))
            {
                Frame.Navigate(typeof(BlankPage2), phpStatus.Text);
            }
        }
    }
}
