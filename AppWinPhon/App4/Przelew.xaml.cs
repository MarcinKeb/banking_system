﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.Web.Http;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace App4
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Przelew : Page
    {
        private HttpClient httpClient;
        private HttpResponseMessage response;
        private String phpAddress = "https://serwer1505592.home.pl/transakcje.php?";
        string id;
        public Przelew()
        {
            this.InitializeComponent();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            this.InitializeComponent();
            httpClient = new HttpClient();
            var headers = httpClient.DefaultRequestHeaders;
            headers.UserAgent.ParseAdd("ie");
            headers.UserAgent.ParseAdd("Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/6.0)");
       }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            id = e.Parameter.ToString();
            textBox.Text = "";
            textBox_Copy.Text = "";
            textBox_Copy1.Text = "";
            textBox_Copy2.Text = "";
            textBox_Copy3.Text = "";
            phpStatus.Text = "";
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(BlankPage2), id);
        }

        private async void button1_Click(object sender, RoutedEventArgs e)
        {
            string nowy_stan_konta_odbiorcy;
            string nowy_stan_konta_nadawcy;
            string nowy_lancuch_nadawcy;
            string nowy_lancuch_odbiorcy;
            string sprawdz;
            string kwota;
            string zmieniona_kwota;
            string Stan_konta_Odbiorcy = "";
            string Stan_konta_Nadawcy = "";


            phpAddress = "https://serwer1505592.home.pl/stanu_konta_nadawcy.php?Idklienta=" + id;
            response = new HttpResponseMessage();

            Uri resourceUri;
            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }

            string responseText;
            //phpStatus.Text = "Waiting for response ...";

            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }
            phpStatus.Text = response.StatusCode + " " + response.ReasonPhrase;
            Stan_konta_Nadawcy = responseText.ToString();

            phpAddress = "https://serwer1505592.home.pl/stanu_konta_odbiorcy.php?Nr_rachunku=" + textBox.Text;
            response = new HttpResponseMessage();
            
            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }
            
            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }
            phpStatus.Text = response.StatusCode + " " + response.ReasonPhrase;
            Stan_konta_Odbiorcy = responseText.ToString();

            nowy_stan_konta_odbiorcy = Convert.ToString(Convert.ToDouble(Stan_konta_Odbiorcy) + Convert.ToDouble(textBox_Copy2.Text));
            for (int i = 0; i < nowy_stan_konta_odbiorcy.Length; i++)
            {
                sprawdz = nowy_stan_konta_odbiorcy.Substring(i, 1);
                if (sprawdz == ",")
                {
                    nowy_lancuch_odbiorcy = nowy_stan_konta_odbiorcy.Remove(i, 1);
                    nowy_stan_konta_odbiorcy = nowy_lancuch_odbiorcy;
                    nowy_lancuch_odbiorcy = nowy_stan_konta_odbiorcy.Insert(i, ".");
                    nowy_stan_konta_odbiorcy = nowy_lancuch_odbiorcy;
                }
            }
            //zmiana przecinka dla bazy danych nadawcy i odjęcie kwoty
            nowy_stan_konta_nadawcy = Convert.ToString(Convert.ToDouble(Stan_konta_Nadawcy) - Convert.ToDouble(textBox_Copy2.Text));
            for (int i = 0; i < nowy_stan_konta_nadawcy.Length; i++)
            {
                sprawdz = nowy_stan_konta_nadawcy.Substring(i, 1);
                if (sprawdz == ",")
                {
                    nowy_lancuch_nadawcy = nowy_stan_konta_nadawcy.Remove(i, 1);
                    nowy_stan_konta_nadawcy = nowy_lancuch_nadawcy;
                    nowy_lancuch_nadawcy = nowy_stan_konta_nadawcy.Insert(i, ".");
                    nowy_stan_konta_nadawcy = nowy_lancuch_nadawcy;
                }
            }
            //zmiana przecinka dla bazy danych kwota
            kwota = textBox_Copy2.Text;
            for (int i = 0; i < kwota.Length; i++)
            {
                sprawdz = kwota.Substring(i, 1);
                if (sprawdz == ",")
                {
                    zmieniona_kwota = kwota.Remove(i, 1);
                    kwota = zmieniona_kwota;
                    zmieniona_kwota = kwota.Insert(i, ".");
                    kwota = zmieniona_kwota;
                }
            }

            DateTime Day = DateTime.Today;
            string data_przelewu = Day.ToString("d");
            phpAddress = "https://serwer1505592.home.pl/transakcje.php?IdKlienta=" + id + "&Nr_rachunku_odbiorcy=" + textBox.Text + "&Imie_odbiorcy=" + textBox_Copy.Text + "&Nazwisko_odbiorcy=" + textBox_Copy1.Text + "&Kwota=" + kwota + "&Tytulem=" + textBox_Copy3.Text + "&Data_transakcji=" + data_przelewu;
            response = new HttpResponseMessage();
                        
            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }

            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }
            phpStatus.Text = response.StatusCode + " " + response.ReasonPhrase;
            phpStatus.Text = responseText.ToString();

            phpAddress = "https://serwer1505592.home.pl/odjecie_pieniedzy_od_nadawcy.php?Idklienta=" + id + "&Stan_konta=" + nowy_stan_konta_nadawcy;
            response = new HttpResponseMessage();

            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }

            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }

            phpAddress = "https://serwer1505592.home.pl/dodanie_pieniedzy_do_odbiorcy.php?Nr_rachunku=" + textBox.Text + "&Stan_konta=" + nowy_stan_konta_odbiorcy;
            response = new HttpResponseMessage();

            if (!Uri.TryCreate(phpAddress.Trim(), UriKind.Absolute, out resourceUri))
            {
                return;
            }
            if (resourceUri.Scheme != "http" && resourceUri.Scheme != "https")
            {
                return;
            }

            try
            {
                response = await httpClient.GetAsync(resourceUri);

                response.EnsureSuccessStatusCode();

                responseText = await response.Content.ReadAsStringAsync();

            }
            catch (Exception ex)
            {
                phpStatus.Text = "Error = " + ex.HResult.ToString("X") +
                    "  Message: " + ex.Message;
                responseText = "";
            }
        }
    }
}
