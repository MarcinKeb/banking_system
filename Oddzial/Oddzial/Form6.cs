﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Oddzial
{
    public partial class Form6 : Form
    {

        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_0000001;password=w7io@O:K,2f,;charset=utf8";
        int id_nadawcy;
        int id_odbiorcy;
        string Nr_rachunku_nadawcy;
        string Imie_nadawcy;
        string Nazwisko_nadawcy;
        double stan_konta;
        public Form6()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeKlienow = "select IdKlienta,Nazwisko,Imie,Pesel,Miasto,Kod_poczt,Ulica,Nr_domu from 18992403_0000001.tbklienci where Aktywny=1 and Pesel like '" + this.textBox1.Text + "%" + "' ;";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlienow, TKlienci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPokazBazeKlientow;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {            
            id_nadawcy = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["IdKlienta"].Value.ToString());
            Imie_nadawcy = dataGridView1.Rows[e.RowIndex].Cells["Imie"].Value.ToString();
            Nazwisko_nadawcy = dataGridView1.Rows[e.RowIndex].Cells["Nazwisko"].Value.ToString();
            MySqlConnection TKonto = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand ZapytanieID = new MySqlCommand("select * from 18992403_0000001.tbKonto where IdKlienta='" + id_nadawcy + "' ;", TKonto);
            MySqlDataReader CzytanieID;
            TKonto.Open();
            CzytanieID = ZapytanieID.ExecuteReader();
            while (CzytanieID.Read())
            {
                Nr_rachunku_nadawcy = CzytanieID.GetString("Nr_rachunku");                
            }
            TKonto.Close();
            textBox2.Enabled = true; textBox3.Enabled = true; textBox4.Enabled = true;
            textBox5.Enabled = true; textBox6.Enabled = true;
            button1.Enabled = true; 
            label2.Enabled = true; label4.Enabled = true; label5.Enabled = true;
            label6.Enabled = true; label7.Enabled = true; label8.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                MySqlConnection TKonto = new MySqlConnection(Polaczenie_z_baza);
                MySqlCommand ZapytanieSprawdz = new MySqlCommand("select * from 18992403_0000001.tbkonto where Nr_rachunku='" + this.textBox2.Text + "' ;", TKonto);
                MySqlDataReader CzytanieNr_rachunku;
                TKonto.Open();
                CzytanieNr_rachunku = ZapytanieSprawdz.ExecuteReader();

                int Pracownik = 0;
                while (CzytanieNr_rachunku.Read())
                {
                    Pracownik += 1;
                }
                if (Pracownik == 1)
                {
                    textBox2.BackColor = Color.Green;
                    id_odbiorcy = CzytanieNr_rachunku.GetInt32("Idklienta");
                    stan_konta = CzytanieNr_rachunku.GetDouble("Stan_konta");
                }
                else
                {
                    textBox2.BackColor = Color.Red;
                }
                TKonto.Close();
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            string nowy_stan_konta;
            string nowy_lancuch;
            string sprawdz;
            string kwota;
            string zmieniona_kwota;

            nowy_stan_konta = Convert.ToString(stan_konta + Convert.ToDouble(textBox5.Text));
            for (int i = 0; i < nowy_stan_konta.Length; i++)
            {
                sprawdz = nowy_stan_konta.Substring(i, 1);
                if (sprawdz == ",")
                {
                    nowy_lancuch = nowy_stan_konta.Remove(i, 1);
                    nowy_stan_konta = nowy_lancuch;
                    nowy_lancuch = nowy_stan_konta.Insert(i, ".");
                    nowy_stan_konta = nowy_lancuch;
                }
            }

            kwota = textBox5.Text;
            for (int i = 0; i < kwota.Length; i++)
            {
                sprawdz = kwota.Substring(i, 1);
                if (sprawdz == ",")
                {
                    zmieniona_kwota = kwota.Remove(i, 1);
                    kwota = zmieniona_kwota;
                    zmieniona_kwota = kwota.Insert(i, ".");
                    kwota = zmieniona_kwota;
                }
            }

            DateTime Day = DateTime.Today;            
            string data_przelewu = Day.ToString("d");
            string ZapytanieDodajKlienta = "insert into 18992403_0000001.tbtransakcje(Nr_rachunku_nadawcy,Imie_nadawcy,Nazwisko_nadawcy,Nr_rachunku_odbiorcy,Imie_odbiorcy,Nazwisko_odbiorcy,Kwota,Tytulem,Data_transakcji) values('" + Nr_rachunku_nadawcy + "','" + Imie_nadawcy + "','" + Nazwisko_nadawcy + "','" + this.textBox2.Text + "','" + this.textBox3.Text + "','" + this.textBox4.Text + "','" + kwota + "','" + this.textBox6.Text + "','" + data_przelewu+ "') ;";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajKlienta = new MySqlCommand(ZapytanieDodajKlienta, TKlienci);
            MySqlDataReader CzytanieKlient;

            try
            {
                TKlienci.Open();
                CzytanieKlient = KwerendaDodajKlienta.ExecuteReader();
                MessageBox.Show("Przelew realizowany");

                while (CzytanieKlient.Read())
                {
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            TKlienci.Close();
            string ZapytanieOdejmijWyplate = "update 18992403_0000001.tbKonto set Stan_konta='" + nowy_stan_konta + "' where Nr_rachunku='" + textBox2.Text + "' ;";
            MySqlConnection TKonto = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaAktualizujStanKonta = new MySqlCommand(ZapytanieOdejmijWyplate, TKonto);
            MySqlDataReader CzytanieKonto;

            try
            {
                TKonto.Open();
                CzytanieKonto = KwerendaAktualizujStanKonta.ExecuteReader();              
                while (CzytanieKonto.Read())
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            FileStream gotowe = new FileStream("Pokwitowanie.pdf", FileMode.Create);
            PdfReader pread = new PdfReader("Pokwitowanie_szablon.pdf");
            PdfStamper pstamp = new PdfStamper(pread, gotowe);
            PdfContentByte Pokwitowanie = pstamp.GetOverContent(1);
            Pokwitowanie.BeginText();
            int lewo = PdfContentByte.ALIGN_LEFT;
            BaseFont font = BaseFont.CreateFont(BaseFont.TIMES_BOLD, BaseFont.CP1250, false);
            Pokwitowanie.SetFontAndSize(font, 14f);
            Pokwitowanie.ShowTextAligned(lewo, textBox2.Text, 15, 400, 0);
            Pokwitowanie.SetFontAndSize(font, 10f);
            Pokwitowanie.ShowTextAligned(lewo, textBox3.Text + " " + textBox4.Text, 15, 373, 0);
            Pokwitowanie.ShowTextAligned(lewo, textBox5.Text, 15, 325, 0);
            Pokwitowanie.ShowTextAligned(lewo, Imie_nadawcy + " " + Nazwisko_nadawcy, 15, 285, 0);
            Pokwitowanie.EndText();
            pstamp.Close();

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "Pokwitowanie.pdf";
            process.Start();
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if ((textBox2.Text != "") && (textBox5.Text != "") && (textBox6.Text != ""))
            {
                button4.Enabled = true;
            }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if ((textBox2.Text != "") && (textBox5.Text != "") && (textBox6.Text != ""))
            {
                button4.Enabled = true;
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            if ((textBox2.Text != "") && (textBox5.Text != "") && (textBox6.Text != ""))
            {
                button4.Enabled = true;
            }
        }
    }
}
