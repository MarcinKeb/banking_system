﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Oddzial
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           timer1.Start();
            dateTimePicker1.Value = DateTime.Now;            
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime Czas = DateTime.Now;
            label2.Text = Czas.ToString(); 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 FormaPierwsza = new Form1();
            FormaPierwsza.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form3 FormaTrzecia = new Form3();
            FormaTrzecia.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form5 FormaPiata = new Form5();
            FormaPiata.ShowDialog();            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form4 FormaCzwarta = new Form4();
            FormaCzwarta.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form6 FormaSzosta = new Form6();
            FormaSzosta.ShowDialog();
        }
    }
}
