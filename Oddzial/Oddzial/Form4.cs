﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace Oddzial
{
    public partial class Form4 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_0000001;password=w7io@O:K,2f,;charset=utf8";
        double Stan_konta = 0;
        public Form4()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeKlienow = "select IdKlienta,Nazwisko,Imie,Data_ur,Pesel,Miasto,Kod_poczt,Ulica,Nr_domu from 18992403_0000001.tbklienci where Aktywny=1 and Pesel like '" + this.textBox1.Text + "%" + "' ;";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlienow, TKlienci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPokazBazeKlientow;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["IdKlienta"].Value.ToString());
            label4.Text = Convert.ToString(id);
            string ZapytaniePokazBazeKlientow = "select * from 18992403_0000001.tbkonto where Idklienta=" + id + "";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlientow, TKlienci);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazeKlientow);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Stan_konta = Convert.ToDouble(dr["Stan_konta"].ToString());
                textBox2.Text = dr["Stan_konta"].ToString() + " PLN";
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            double suma = 0;
            int banknot200 = 0; int banknot100 = 0; int banknot50 = 0;
            int banknot20 = 0; int banknot10 = 0;
            int moneta5 = 0; int moneta2 = 0; int moneta1 = 0;
            int moneta50gr = 0; int moneta20gr = 0; int moneta10gr = 0;
            int moneta5gr = 0; int moneta2gr = 0; int moneta1gr = 0;

            suma = Convert.ToDouble(textBox3.Text);
            while (suma >= 200)
            {
                banknot200++;
                suma -= 200;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 100)
            {
                banknot100++;
                suma -= 100;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 50)
            {
                banknot50++;
                suma -= 50;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 20)
            {
                banknot20++;
                suma -= 20;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 10)
            {
                banknot10++;
                suma -= 10;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 5)
            {
                moneta5++;
                suma -= 5;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 2)
            {
                moneta2++;
                suma -= 2;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 1)
            {
                moneta1++;
                suma -= 1;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 0.50)
            {
                moneta50gr++;
                suma -= 0.50;
                suma = Math.Round(suma, 2);

            }
            while (suma >= 0.20)
            {
                moneta20gr++;
                suma -= 0.20;
                suma = Math.Round(suma, 2);               
            }
            while (suma >= 0.10)
            {
                moneta10gr++;
                suma -= 0.10;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 0.05)
            {
                moneta5gr++;
                suma -= 0.05;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 0.02)
            {
                moneta2gr++;
                suma -= 0.02;
                suma = Math.Round(suma, 2);
            }
            while (suma >= 0.01)
            {
                moneta1gr++;
                suma -= 0.01;
                suma = Math.Round(suma, 2);
            }

            Document doc = new Document(iTextSharp.text.PageSize.LETTER, 10, 10, 42, 35);
            PdfWriter wri = PdfWriter.GetInstance(doc, new FileStream("Wyplata.pdf", FileMode.Create));
            doc.Open();
            for (int i = 0; i < banknot200; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("200.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < banknot100; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("100.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < banknot50; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("50.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < banknot20; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("20.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < banknot10; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("10.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta5; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta5.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta2; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta2.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta1; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta1.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta50gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta50gr.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta20gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta20gr.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta10gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta10gr.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta5gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta5gr.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta2gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta2gr.jpg");
                doc.Add(gif);
            }
            for (int i = 0; i < moneta1gr; i++)
            {
                iTextSharp.text.Image gif = iTextSharp.text.Image.GetInstance("Moneta1gr.jpg");
                doc.Add(gif);
            }
            doc.Close();
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "Wyplata.pdf";
            process.Start();

            string nowy_stan_konta;
            string nowy_lancuch;
            string sprawdz;
            
            nowy_stan_konta = Convert.ToString(Stan_konta - Convert.ToDouble(textBox3.Text));
            for (int i = 0; i < nowy_stan_konta.Length; i++)
            {
                sprawdz = nowy_stan_konta.Substring(i, 1);
                if (sprawdz == ",")
                {
                    nowy_lancuch = nowy_stan_konta.Remove(i, 1);
                    nowy_stan_konta = nowy_lancuch;
                    nowy_lancuch = nowy_stan_konta.Insert(i, ".");
                    nowy_stan_konta = nowy_lancuch;
                }
            }
            string ZapytanieDodajWplate = "update 18992403_0000001.tbkonto set Stan_konta='" + nowy_stan_konta + "' where Idklienta='" + this.label4.Text + "' ;";
            MySqlConnection TKonto = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajWplate = new MySqlCommand(ZapytanieDodajWplate, TKonto);
            MySqlDataReader CzytanieKonto;
            try
            {
                TKonto.Open();
                CzytanieKonto = KwerendaDodajWplate.ExecuteReader();                
                while (CzytanieKonto.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();
        }
    }
}
