﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Oddzial
{
    public partial class Form5 : Form
    {
        string Polaczenie_z_baza = "datasource=serwer1505592.home.pl;port=3306;username=18992403_0000001;password=w7io@O:K,2f,;charset=utf8";
        double Stan_konta = 0;
        public Form5()
        {
            InitializeComponent();
        }
        void load_table_klienci()
        {
            string ZapytaniePokazBazeKlientow = "select IdKlienta,Nazwisko,Imie,Data_ur,Pesel,Miasto,Kod_poczt,Ulica,Nr_domu from 18992403_0000001.tbklienci where Aktywny=1;";
            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlientow, TKlienci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPodazBazeKlientow;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();

                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            load_table_klienci();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            string ZapytaniePokazBazeKlienow = "select Nazwisko,Imie,Pesel,Data_ur,Miasto,Kod_poczt,Ulica,Nr_domu,IdKlienta from 18992403_0000001.tbklienci where Aktywny=1 and Pesel like '" + this.textBox1.Text + "%" + "' ;";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPokazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlienow, TKlienci);
            try
            {
                MySqlDataAdapter sda = new MySqlDataAdapter();
                sda.SelectCommand = KwerendaPokazBazeKlientow;
                DataTable dbdataset = new DataTable();
                sda.Fill(dbdataset);
                BindingSource bSource = new BindingSource();
                bSource.DataSource = dbdataset;
                dataGridView1.DataSource = bSource;
                sda.Update(dbdataset);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int id;
            id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells["IdKlienta"].Value.ToString());
            label4.Text = Convert.ToString(id);
            string ZapytaniePokazBazeKlientow = "select * from 18992403_0000001.tbkonto where Idklienta=" + id + "";

            MySqlConnection TKlienci = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaPodazBazeKlientow = new MySqlCommand(ZapytaniePokazBazeKlientow, TKlienci);
            DataTable dt = new DataTable();
            MySqlDataAdapter da = new MySqlDataAdapter(KwerendaPodazBazeKlientow);
            da.Fill(dt);
            foreach (DataRow dr in dt.Rows)
            {
                Stan_konta = Convert.ToDouble(dr["Stan_konta"].ToString());
                textBox2.Text = dr["Stan_konta"].ToString() + " PLN";                
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            string nowy_stan_konta;
            string nowy_lancuch;
            string sprawdz;

            nowy_stan_konta = Convert.ToString(Stan_konta + Convert.ToDouble(textBox3.Text));
            for (int i = 0; i < nowy_stan_konta.Length; i++)
            {
                sprawdz = nowy_stan_konta.Substring(i, 1);
                if (sprawdz == ",")
                {
                    nowy_lancuch = nowy_stan_konta.Remove(i, 1);
                    nowy_stan_konta = nowy_lancuch;
                    nowy_lancuch = nowy_stan_konta.Insert(i, ".");
                    nowy_stan_konta = nowy_lancuch;
                }
            }
            string ZapytanieDodajWplate = "update 18992403_0000001.tbkonto set Stan_konta='" + nowy_stan_konta + "' where Idklienta='" + this.label4.Text + "' ;";
            MySqlConnection TKonto = new MySqlConnection(Polaczenie_z_baza);
            MySqlCommand KwerendaDodajWplate = new MySqlCommand(ZapytanieDodajWplate, TKonto);
            MySqlDataReader CzytanieKonto;
            try
            {
                TKonto.Open();
                CzytanieKonto = KwerendaDodajWplate.ExecuteReader();
                MessageBox.Show("Zaksięgowano wpłatę");
                while (CzytanieKonto.Read())
                {
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 FormaDruga = new Form2();
            FormaDruga.ShowDialog();
        }
    }
}
